const { getField, updateField }  =  require('vuex-map-fields') ;
const SET_COMMENT_FIELD = 'SET_COMMENT_FIELD'
const GET_COMMENT_FIELD = 'GET_COMMENT_FIELD'
const ADD_TO_COMMENTS_LIST = 'ADD_TO_COMMENTS_LIST'
const GET_COMMENTS_LIST = 'GET_COMMENTS_LIST'
const state = {
    commentField:'',
    commentsList:[]
}
const mutations = {
    updateField,
    [SET_COMMENT_FIELD]:(state:any,payload:any)=>{state.commentField = payload},
    [ADD_TO_COMMENTS_LIST]:(state:any,payload:any)=>{
        state.commentsList = [...state.commentsList,payload]
    }
}
const actions = {
    [SET_COMMENT_FIELD]:({commit}:any,payload:any)=>{
        commit(SET_COMMENT_FIELD,payload)
    },
    [ADD_TO_COMMENTS_LIST]:({commit,state}:any)=>{
        let options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' ,hour: 'numeric', hour12: true,minute:'numeric'};
        let now  = new Date();
        commit(ADD_TO_COMMENTS_LIST,{name:state.commentField,date:now.toLocaleDateString("en-US", options)});
    }
}
const getters = {
    getField,
    [GET_COMMENT_FIELD]:(state:any)=>{return state.commentField},
    [GET_COMMENTS_LIST]:(state:any)=>{return state.commentsList},
}

export default {
    namespaced:true,
    state,mutations,actions,getters
}