import Vue from 'vue'
import Vuex from 'vuex'
import comments from './modules/comments'

Vue.use(Vuex)

const store =  new Vuex.Store({
    modules:{
        comments
    },
    strict:true
})

export default store
