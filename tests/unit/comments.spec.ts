import { shallowMount,createLocalVue  } from '@vue/test-utils';
import CommentBox from '@/components/CommentBox.vue';
import CommentList from '@/components/CommentList.vue';
import Reddit from '@/views/Reddit.vue'
import Vuex from 'vuex'
import store from '@/store'
const localVue = createLocalVue()
const commentBoxwrapper = shallowMount(CommentBox,{store});
localVue.use(Vuex)
describe('CommentBox Module', () => {

    it('should have comment box component', () => {
      expect(commentBoxwrapper).toBeTruthy();
    });
    it('should have comment box text area', () => {
        expect(commentBoxwrapper.html()).toContain('textarea');
    });

    it('should have comment button', () => {
        expect(commentBoxwrapper.html()).toContain('button');
    });
    it('should add to comments list on dispatch of add comment action', () => {
        let _comments = store.getters['comments/GET_COMMENTS_LIST'];
        store.dispatch('comments/ADD_TO_COMMENTS_LIST',{name:'Foo'});
        expect(store.getters['comments/GET_COMMENTS_LIST'].length).toBe(_comments.length+1);
    });
    it('should add to comments list on click of a button', () => {
        let _comments = store.getters['comments/GET_COMMENTS_LIST'];
        const btn = commentBoxwrapper.find('button');
        btn.trigger('click')
        expect(store.getters['comments/GET_COMMENTS_LIST'].length).toBe(_comments.length+1);
    });
    it('should have comment list component', () => {
        const listWrapper = shallowMount(CommentList,{store});
        expect(listWrapper).toBeTruthy();
    });
    it('should have a list ready for comments', () => {
        const listWrapper = shallowMount(CommentList,{store});
        expect(listWrapper.html()).toContain('ul');
    });
    it('should render added comment to the list', () => {
        const listWrapper = shallowMount(CommentList,{store});
        let liCount = listWrapper.findAll('li').length;
        const btn = commentBoxwrapper.find('button');
        btn.trigger('click')
        expect(listWrapper.findAll('li').length).toBe(liCount+1);
    });
    it('Reddit Component should have comment list component', () => {
        const redditWrapper = shallowMount(Reddit,{store});
        expect(redditWrapper.find(CommentList).exists()).toBe(true);
    });
  });